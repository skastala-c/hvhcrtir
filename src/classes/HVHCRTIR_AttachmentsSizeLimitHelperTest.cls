@isTest
private class HVHCRTIR_AttachmentsSizeLimitHelperTest{

    static testMethod void invalidAtthmntMehod() {
        
        test.startTest();
        
        HVHCRTIR_Valid_Attachment_check__c validAttachCs = new HVHCRTIR_Valid_Attachment_check__c(Name = 'Ticket attachment criteria',
                                                                                                 Supported_file_size__c = 1,
                                                                                                 Supported_file_types__c = 'jpeg,pdf,png,docx,zip');

        insert validAttachCs;
        
        Contact newContact = TestDataFactory.createContact(1)[0];
        insert newContact;
        
        Case newCase = TestDataFactory.createcase(1)[0];
        insert newCase;
        
        List<EmailMessage> mails = TestDataFactory.createEmail(3);
        insert mails;
        
        list<Attachment> attachments = new List<attachment>();
        
        Blob b = Blob.valueOf('Test Data');        
        Attachment attachment = new Attachment();
        attachment.ParentId = mails[0].id;
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = b;        
        attachments.add(attachment);
        
        Blob b1 = Blob.valueOf('Test Data');        
        Attachment attachment1 = new Attachment();
        attachment1.ParentId = mails[1].id;
        attachment1.Name = 'Test Attachment for Parent.jpeg';
        attachment1.Body = b;        
        attachments.add(attachment1); 
        
        insert attachments;
        
        test.stopTest();
        
    }
}