public class DisplayCaseController {
     
    @AuraEnabled
    public static Case getCaseFromId(Id caseID) {
        system.debug('First line caseId    '+caseID);
        List<Case> cases=new List<Case>();
        if(caseID == null) {
            return [SELECT Id, CaseNumber,OwnerId,Origin,Priority,Status,Subject,Type,Description,Reason,AccountId,ContactId,ContactPhone,ContactEmail,ContactMobile,recordTypeId,Categorization__c,Categorization_FEP_Membership__c,CCP_Partner_with_PFC_Prof_Affairs__c,CCP_Provider_Number__c,CCP_Provider_Resolution__c,CCP_Provider_Type__c,CCPriority_Challenge__c,CCPriority_FollowUp_Action__c,CCPriority_Last_Action_Taken__c,CCPriority_Resolution_FollowUp__c,DSA_Amount__c,HVHC_TicketNumber__c,Inquiry_Sent__c,Inquiry_Sent_To__c,Invoice__c,Quality_Review_1__c,Quality_Review_2__c,Response_Received__c from Case LIMIT 1];
        }else{        
          cases = [SELECT Id, CaseNumber,OwnerId,Origin,Priority,Status,Subject,Type,Description,Reason,AccountId,ContactId,ContactPhone,ContactEmail,ContactMobile,recordTypeId,Categorization__c,Categorization_FEP_Membership__c,CCP_Partner_with_PFC_Prof_Affairs__c,CCP_Provider_Number__c,CCP_Provider_Resolution__c,CCP_Provider_Type__c,CCPriority_Challenge__c,CCPriority_FollowUp_Action__c,CCPriority_Last_Action_Taken__c,CCPriority_Resolution_FollowUp__c,DSA_Amount__c,HVHC_TicketNumber__c,Inquiry_Sent__c,Inquiry_Sent_To__c,Invoice__c,Quality_Review_1__c,Quality_Review_2__c,Response_Received__c from CASE where ID = :caseID];
        } 
        system.debug('Case details'+cases[0]);
        return cases[0];
           
    }
    
}