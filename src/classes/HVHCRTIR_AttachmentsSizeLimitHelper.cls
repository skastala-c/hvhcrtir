/********************************************************************
* Name:        HVHCRTIR_AttachmentsSizeLimitHelper 
* Description:  Holds logic to validate attachment in email message
* ------------------------------------------------------------------
* Developer             HVHC      Date         Description
* -------------------------------------------------------------------
* Swapna                        19/09/2017      Created
********************************************************************/


public class HVHCRTIR_AttachmentsSizeLimitHelper{
@future
    public static void attSizeLimitAndCallout(list<id> emids){
        // variable Declaration
        map<id,EmailMessage> emmap= new map<id,EmailMessage>();
        map<id,EmailMessage> caseToFromAddressMap = new map<id,EmailMessage>();
        map<id,integer> caseIdtoEmailCount = new map<id,integer>();
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
      
        map<id,EmailMessage> emailsMsgsMap = new map<id,EmailMessage>();
        list<EmailMessage> emailMsgstoDelete = new list<EmailMessage>();
        map<id,id> emattIds = new map<id,id>();
        set<id> casesetToDelete=new set<id>();
        list<case> caselistToDelete = new List<case>();
        list<case> caselistToUpdate=new list<case>();
        
        //Fetching contact to use it in targetobjectid while sending email
        contact con = [select id from contact where email != null limit 1];
        
      
        for(EmailMessage em:[SELECT Id,FromAddress,HasAttachment,parentid,parent.Incoming_Email_Addres__c FROM EmailMessage where id=:emids]){
           // Map to store emailmessage id and email message
            if(em.HasAttachment == True){
                 emmap.put(em.id,em);
            }else{
                caseToFromAddressMap.put(em.parentid,em);
            }
        }   
        
        for(id temp:caseToFromAddressMap.keyset()){
                case caseToUpdate = new case();
                caseToUpdate.id = temp;
                caseToUpdate.EmailWithoutAttachment__c = True;
                caseToUpdate.Incoming_Email_Addres__c =  caseToFromAddressMap.get(temp).FromAddress;
                caselistToUpdate.add(caseToUpdate);
        }
        if(caselistToUpdate.size() > 0){
           update caselistToUpdate;
        }
       
            // Query to fetch attachment whose parentid is in map key set 
            for (Attachment att : [Select id,body,parentid,name from Attachment where parentid =: emmap.keySet()]) {
             system.debug('att.body.size()' +att.body.size());
            // Email attachment validation
             if(!isValidAttachment(att))
                {             
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                EmailTemplate eid=[Select id from EmailTemplate where name='HVHC_NotificationEmailOnSizeLimit'];
                mail.setTemplateId(eid.id);
                mail.setTargetObjectId(con.id);
                mail.setTreatTargetObjectAsRecipient(false);
                mail.setSaveAsActivity(false);
                String[]toaddresses=new String[]{emmap.get(att.ParentId).FromAddress};
                mail.setToAddresses(ToAddresses);
                allmsg.add(mail);
              //  atts.add(att);
                system.debug('sent email');                        
              //  case cs=new case();
             //   cs.id=emmap.get(att.parentid).parentid;
                emailMessage em = new emailMessage();
                em.id = att.parentid;
                emailsMsgsMap.put(emmap.get(att.parentid).parentid,em);
                casesetToDelete.add(emmap.get(att.parentid).parentid);   
            } 
             else{            
                emattIds.put(att.parentid,att.id); 
                system.debug('callout');
            }  
            }
        if(allmsg.size()>0){// && atts.size()>0){
                Messaging.SendEmailResult[] results = Messaging.sendEmail(allmsg);
                if (results[0].success) {
                    System.debug('The email was sent successfully.');
                } else {
                    System.debug('The email failed to send: '+results[0].errors[0].message);
                }
                 
          //  Delete(atts);  
         caseIdtoEmailCount = countEmailsOnCase(casesetToDelete);
            for(id temp :caseIdtoEmailCount.keyset()){
                if(caseIdtoEmailCount.get(temp) <= 1){
                    case cs = new case();
                    cs.id = temp;
                    caselistToDelete.add(cs);
                }else{
                    emailMsgstoDelete.add(emailsMsgsMap.get(temp));
                }           
            }
             //caselistToDelete.addAll(casesetToDelete);
            if(caselistToDelete.size() > 0){
               Delete caselistToDelete; 
            }
            if(emailMsgstoDelete.size() > 0){
               Delete emailMsgstoDelete;
            }
             system.debug('delete att');          
        }
    }
    
 // Method to validate attachment
 public static boolean isValidAttachment(Attachment att){
        
    // Variable declaration
     boolean isNotValidType = true;
     boolean isValidSize = true;
        
    // fetch record from custom settting
     HVHCRTIR_Valid_Attachment_check__c validAttachCs = HVHCRTIR_Valid_Attachment_check__c.getinstance('Ticket attachment criteria');
     list<String> validFormats = validAttachCs.Supported_file_types__c.split(',');
     if(att.body.size()/1048576>=validAttachCs.Supported_file_size__c){
            isValidSize = false;
     }else{
         for(string temp:validFormats){
             if(att.Name.endsWithignoreCase(temp)){
                 isNotValidType = false;
             }
         } 
     }
      return isValidSize && !isNotValidType ;   
    }     
    
// Method to return number of email messages associated to case 
public static map<id,integer> countEmailsOnCase(Set<id> caseIds){
      map<id,integer> caseIdtoEmailCount = new map<id,integer>();
        for(AggregateResult result : [SELECT COUNT(Id) sum,ParentId caseId FROM EmailMessage where incoming = true and ParentId in :caseIds group by parentid]){
              caseIdtoEmailCount.put((id)result.get('caseId'),(Integer)result.get('sum'));     
              
        }         
        return caseIdtoEmailCount;               
    }
}