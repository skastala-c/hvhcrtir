public class HVHCRTIR_CaseTriggerAssignmentHandler{

public void  roundRobinAssignment(List<case> newCaseList,Map<Id,case> oldCaseMap){
       Map<Id,Case> caseWithQueue=new Map<Id,Case>();
       Set<Id> groupQueueIds=new Set<Id>();
       for(Case cs: newCaseList){
           if(cs.ownerid.getSobjectType()==Schema.Group.SObjectType){
               caseWithQueue.put(cs.id,cs);
               groupQueueIds.add(cs.ownerid);
           }
       }
       
       Map<Id,List<Id>> queuIdCaseIds=new Map<Id,List<Id>>();
       
       for(String queueRelatedId: groupQueueIds){
            List<Id> caseIds=new List<Id>();
            for(Case cs: caseWithQueue.values()){
                if(cs.ownerid==queueRelatedId){
                  caseIds.add(cs.id);
                }
            }
         queuIdCaseIds.put(queueRelatedId,caseIds);
         
       }
       
       for(Id keyId: queuIdCaseIds.keySet()){
            HVHCRTIR_RoundRobinHandler.roundRobinFuture(keyId,queuIdCaseIds.get(keyId));
       }
}

/*
 Author : Shikher Goel
 Description : This method is written because we need to change the Case Record Type whenever Case Owner changes.
               We should have Each Record Type mapped with Each Case Queue such that we can assign the Correct Record Type based on Case Owner
               
*/
 public void  assignRecordTypes(List<case> newCaseList,Map<Id,case> oldCaseMap){
   List<Case> listOfCasesOwnerChangesToQueue = new List<Case>();
   List<Id> listOfQueueIdsWhereCasesOwnerChangesToQueue = new List<String>();
   List<Case> listOfCasesOwnerChangesToUsers = new List<Case>();
   
   for(Case newCase : newCaseList)
      {
       if(oldCaseMap.get(newCase.Id).OwnerId != newCase.OwnerId)
         {
           if(newCase.ownerid.getSobjectType()==Schema.Group.SObjectType){
              listOfCasesOwnerChangesToQueue.add(newCase);
              listOfQueueIdsWhereCasesOwnerChangesToQueue.add(newCase.OwnerId);
           }
           else
           {
              listOfCasesOwnerChangesToUsers.add(newCase);
           }
         }
      }
   // Do the logic when case owner changes to Queue.
   if(listOfQueueIdsWhereCasesOwnerChangesToQueue.size() > 0)
     {
       Map<Id,Group> mapOfQueues = new Map<Id,Group>([SELECT Id,Name FROM Group Where Type = 'Queue' AND Id IN :listOfQueueIdsWhereCasesOwnerChangesToQueue]);
       
       List<RecordType> listOfRecordTypes = [SELECT Id,Name FROM RecordType WHERE SobjectType = 'Case'];
       
       Map<String,Id> mapOfCaseRecordTypeNameToId = new Map<String,Id>(); 
       for(RecordType caseRecordType : listOfRecordTypes)
          {
           mapOfCaseRecordTypeNameToId.put(caseRecordType.Name,caseRecordType.Id);
          }
         
       for(Case caseOwnerHavingQueue: listOfCasesOwnerChangesToQueue)
          {
           Group caseOwnerQueue = mapOfQueues.get(caseOwnerHavingQueue.OwnerId);
           Id recordTypeId = mapOfCaseRecordTypeNameToId.get(caseOwnerQueue.Name);
           caseOwnerHavingQueue.RecordTypeId = recordTypeId;
          }         
     }
 }
}