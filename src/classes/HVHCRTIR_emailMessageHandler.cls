/********************************************************************************************************************************************************
* Name:        HVHCRTIR_emailMessageHandler 
* Description: 1)When case gets created through email-to-case  and if case dont have account and contact information associated to it this calss will be
               invoked                               
               2)By reading email body it will capture the account information,from email address 
               3)creats account and contact 
               4)map's the account and contact information on the case.
* -------------------------------------------------------------------------------------------------------------------------------------------------------
* Developer             HVHC      Date         Description
* -------------------------------------------------------------------------------------------------------------------------------------------------------
* Pichikala JayaLakshmi         23/08/2017      Created
*********************************************************************************************************************************************************/
public class HVHCRTIR_emailMessageHandler {

    @future
    public static void mapData(Map<id,id> caseIdEmailMsgIdMap){
      
     
      Map<ID, Case>  caseData=new Map<ID, Case>([select id, casenumber, accountid,contactid from Case where id in: caseIdEmailMsgIdMap.keySet()]);
      Map<ID, EmailMessage>  emailMessageData=new Map<ID, EmailMessage>([SELECT Id, ParentId, Subject, FromName, FromAddress, ToAddress, CcAddress, BccAddress, Status, TextBody FROM EmailMessage where id in :caseIdEmailMsgIdMap.values()]);
      
      //To Add New Contacts
      List<Contact> addContacts=new List<Contact>();    
         
      Map<Id,List<String>> emailMsgContactName=new Map<Id,List<String>>();
      Map<Id,String> emailMsgIDemail=new Map<Id,String>();
      
      
      // Read the emailMessaged data and create the account and contact
      for(EmailMessage em: emailMessageData.values()){                  
                                       
                     //Add contactand email message details to map
                     List<String> contactDetails=new List<String>();
                     contactDetails.add(em.FromAddress);
                     contactDetails.add(em.FromName);                    
                     emailMsgContactName.put(em.id,contactDetails);
                     emailMsgIDemail.put(em.id,em.FromAddress);
       
      }
      
      Set<String> emailStoCreateContact=new Set<String>();
      emailStoCreateContact.addAll(emailMsgIDemail.values());
      
      // Check do we have contacts exists with email id, filter the date exists in the system and create new contacts
      for(Contact cnt:[select id, email, lastname from contact where email in: emailMsgIDemail.values()]){     
         if(emailStoCreateContact.contains(cnt.email)) emailStoCreateContact.remove(cnt.email);
      }      
       
     
      //insert contact 
      for(List<String> lst: emailMsgContactName.values()){
          if(emailStoCreateContact.contains(lst[0])){
               Contact cnt=new Contact();
               cnt.email=lst[0];
               cnt.lastName=lst[1]; 
               addContacts.add(cnt);
           }
      
      }
      if(addContacts.size()>0)  insert addContacts;
      
      system.debug('addContacts :::'+addContacts);
      
      List<Contact> cnts=[select id,LastName,AccountId,email from Contact where email in : emailStoCreateContact];
 
      Map<String,Map<String,String>> emailNContactDetails=new Map<String,Map<String,String>>();      
      Map<String,String> contactIdNEmail=new Map<String, String>();
      Map<String,String> IdNEmail=new Map<String, String>();
      Map<String,String> contactEmailcontactID=new Map<String, String>();
      
      for(Contact cnt:cnts){
           Map<String,String> accNCnt=new Map<String,String>();          
           accNCnt.put('ContactId',cnt.id);
           accNCnt.put('ContactEmail',cnt.email);
                      
           emailNContactDetails.put(cnt.id,accNCnt);
           contactIdNEmail.put(cnt.id+'&&&'+cnt.email,cnt.email);           
           IdNEmail.put(cnt.id,cnt.email);
           contactEmailcontactID.put(cnt.email,cnt.id);
      }
      
      // Now fill all the cases with contact reading emailMessage
      List<Case> updateCases=new List<Case>();
      for(Id casekeyId:caseIdEmailMsgIdMap.keySet()){
         Case cs=caseData.get(casekeyId);
         EmailMessage em=emailMessageData.get(caseIdEmailMsgIdMap.get(casekeyId));
         
         List<String> contactLstDetails =emailMsgContactName.get(caseIdEmailMsgIdMap.get(casekeyId));
         String fromemailAdress=contactLstDetails[0];
         String FromName=contactLstDetails[1];         
         cs.contactId=contactEmailcontactID.get(fromemailAdress); 
         
         updateCases.add(cs);         
         
      }      
      update updateCases; 
    }
}