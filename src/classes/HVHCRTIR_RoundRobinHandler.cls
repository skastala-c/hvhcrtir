public class HVHCRTIR_RoundRobinHandler{
@future
public static void roundRobinFuture(String queueId,List<Id> caseIds){
   Id recordTypeIdCase=[select id, recordTypeId from case where id in : caseIds][0].recordTypeId;
   List<GroupMember> groupMembers=[SELECT UserOrGroupId, SystemModstamp, Id, GroupId FROM GroupMember where GroupId =: queueId];
   List<Id> groupMemberIds=new List<Id>();
   for(GroupMember gm: groupMembers){
       groupMemberIds.add(gm.UserOrGroupId);
   }
   
   AggregateResult[] groupedResults =[select ownerid ,count(id) from case where recordTypeId = : recordTypeIdCase and ownerid in :groupMemberIds group by ownerid  order by count(id) ];
   List<Id> groupMemberId=new List<Id>();
   Map<Id,Decimal>  ownerCountCases=new Map<Id,Decimal>();
   for (AggregateResult ar : groupedResults)  {
        ownerCountCases.put((Id)ar.get('ownerid'),(Decimal)ar.get('expr0')); 
        groupMemberId.add((Id)ar.get('ownerid'));     
   }
   
   //Handling queue members with zero cases  
   List<Id> zeroGroupMemberId=new List<Id>();
   for(Id queueMemberId:groupMemberIds){
       if(!ownerCountCases.containsKey(queueMemberId)){
         ownerCountCases.put(queueMemberId,0);
         zeroGroupMemberId.add(queueMemberId);
       }
   }
   
   //final list of users for assiging the case
   List<Id> memberOwnerIds=new List<Id>();
   memberOwnerIds.addAll(zeroGroupMemberId);
   memberOwnerIds.addAll(groupMemberId);
   
   system.debug('memberOwnerIds :::'+memberOwnerIds);
   
   Integer i=0;
   List<Case> updateCases=new List<Case>();
   for(Case cs:[select id, ownerid from case where id in :caseIds]){
       
       cs.ownerid=memberOwnerIds[i];updateCases.add(cs);
       i++;
       if(i==memberOwnerIds.size()) i=0;
   }
   
   update updateCases;
   
   
}
}