/*******************************************************************
* Name:        TestDataFactory 
* Description: Common Test Utility Classes for Test Data Creation
* ------------------------------------------------------------------
* Developer             HVHC      Date         Description
* -------------------------------------------------------------------
* Swapna                        19/09/2017      Created
**********************************************************************/

public abstract class TestDataFactory  {
    
    // Contact Creation
     public static List<Contact> createContact( Integer numberOfContacts) {
        List<Contact> Contacts = new List<Contact>();
        for ( Integer i = 0 ; i < numberOfContacts ; i++ ) {
            
            Contact newContact = new Contact( firstname = 'Test Account' + Math.random(), lastname = 'Account',
                                              email = 'noreplay@email.com');
            Contacts.add( newContact);

        }
        return Contacts;

    }
    
    // Case Creation
     public static List<case> createcase( Integer numberOfCases) {
        List<Case> cases = new List<case>();
        for ( Integer i = 0 ; i < numberOfCases ; i++ ) {
            
            Case newcase = new Case( Subject = 'On Test' + Math.random(),
                                  Status ='New',
                                  priority = 'Medium',
                                  Origin = 'Email');
            cases.add(newcase);

        }
        return cases;

    }
    
    // EmailMessage creatiom
    public static List<EmailMessage> createEmail( Integer numberOfEmailMessages) {
        Case c = new Case();
        c.Subject = 'On Test'+math.random();
        c.Status ='New';
        c.Priority = 'Medium';
        c.Origin = 'Email';
        insert c;
        
        List<EmailMessage> EmailMessages = new List<EmailMessage>();
        for ( Integer i = 0 ; i < numberOfEmailMessages ; i++ ) {
            
            EmailMessage newemailMessage = new EmailMessage(FromAddress = 'test@fromaddress.com',
                                                            FromName = 'Test Sender',
                                                            Incoming = True,
                                                            ToAddress= 'test@toaddress.com',
                                                            Subject = 'Test email' +math.random(),
                                                            HtmlBody = 'Test email body',
                                                            ParentId = c.Id); 
                                                           
            EmailMessages.add(newemailMessage);

        }
        return EmailMessages;

    }

}