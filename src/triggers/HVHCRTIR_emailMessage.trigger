/********************************************************************************************************************************************************
* Name:        HVHCRTIR_emailMessage 
* Description: 1)When email messge is received based on email-to-case settigs
               2)Email message creates a case  
               3)if the case is not associated with contact, it invokes the HVHCRTIR_emailMessageHandler 
               class which will take care of associating contact details to case.
* -------------------------------------------------------------------------------------------------------------------------------------------------------
* Developer             HVHC      Date         Description
* -------------------------------------------------------------------------------------------------------------------------------------------------------
* Pichikala JayaLakshmi         23/08/2017      Created
*********************************************************************************************************************************************************/
trigger HVHCRTIR_emailMessage on EmailMessage (after insert) {

Map<id,id> caseIdnEmailMsgId=new Map<id,id>();
    for(EmailMessage em:trigger.new){
        caseIdnEmailMsgId.put(em.parentId,em.id);
    }
    
// Get all the case details based on the emailmessages.
List<Case>  caseData=[select id, casenumber, accountid,contactId from case where id in: caseIdnEmailMsgId.keySet()];

for(Case cs:caseData){
  //If we account is associated with the case , then remove the case details from the map.
  if(cs.contactId!=null) caseIdnEmailMsgId.remove(cs.id);
}


/*
1)If caseIdnEmailMsgId size is greater than zero indicates to call the apex class to map the data
2)Following cases dont have accountid's associated to them
3)Send all the case and emailMessages to the apex to map the data accordingly
*/

if(caseIdnEmailMsgId.size()>0){
    HVHCRTIR_emailMessageHandler.mapData(caseIdnEmailMsgId);
}

}