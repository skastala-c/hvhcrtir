trigger HVHCRTIR_EmailMessageObjectTrigger on EmailMessage (after insert) {    
     
    list<string> ids=new list<string>();
    if(trigger.isafter&& trigger.isinsert){
        for(EmailMessage em:trigger.new){
            if(em.Incoming == true){          
                ids.add(em.id);
            }
        }
        if(ids.size()>0){       
           HVHCRTIR_AttachmentsSizeLimitHelper.attSizeLimitAndCallout(ids);         
        }
    }
}