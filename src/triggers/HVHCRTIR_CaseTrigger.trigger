trigger HVHCRTIR_CaseTrigger on Case (after insert,before update) {
  HVHCRTIR_CaseTriggerAssignmentHandler assignToUser=new HVHCRTIR_CaseTriggerAssignmentHandler();
  //if(Trigger.isInsert)
  //  assignToUser.roundRobinAssignment(Trigger.new, Trigger.newMap);
  if(Trigger.isBefore && Trigger.isUpdate)
     assignToUser.assignRecordTypes(Trigger.new, Trigger.OldMap);
}