trigger HVHC_ApprovalProcess on Case (after update) {
    for (Case cs : Trigger.new){
        Approval.ProcessSubmitRequest req=new Approval.ProcessSubmitRequest();
        req.setObjectId(cs.id);
        Approval.ProcessResult res=Approval.process(req);        
    }
}