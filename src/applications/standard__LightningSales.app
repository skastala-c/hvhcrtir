<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Case_Record_Page3</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Case</pageOrSobjectType>
    </actionOverrides>
    <formFactors>Small</formFactors>
    <formFactors>Medium</formFactors>
    <formFactors>Large</formFactors>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Feed</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Task</tab>
    <tab>standard-File</tab>
    <tab>standard-ContentNote</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>standard-Feed</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-Event</tab>
    <tab>standard-OtherUserProfile</tab>
    <tab>standard-Case</tab>
    <tab>standard-News</tab>
    <tab>standard-Forecasting3</tab>
    <tab>standard-Quote</tab>
    <uiType>Lightning</uiType>
</CustomApplication>
